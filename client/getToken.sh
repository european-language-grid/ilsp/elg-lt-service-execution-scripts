#!/bin/bash

username=$1
password=$2
instance=$3

echo "username:"$username 
echo "password:"$password
echo "instance:"$instance

cmd="curl --location --request POST '${instance}/auth/realms/ELG/protocol/openid-connect/token' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'client_id=react-client' --data-urlencode 'grant_type=password' --data-urlencode 'username=$username' --data-urlencode 'password=$password'"

echo $cmd
RESULT=`eval $cmd`
echo $RESULT

echo "\n"
echo "* Recovery of the token"
TOKEN=`echo $RESULT | sed 's/.*access_token":"//g' | sed 's/".*//g'` 
echo $TOKEN > token.txt

cat token.txt
