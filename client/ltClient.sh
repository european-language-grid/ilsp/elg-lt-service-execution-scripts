#!/bin/bash

clear
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

ltsrv=""
oper=""
endpoint=""
auth=""
file=""
cmd=""
params=""

# Parse arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -ltsrv)
    ltsrv="$2"
    shift # past argument
    shift # past value
    ;;
    -op)
    oper="$2"
    shift # past argument
    shift # past value
    ;;
    -end)
    endpoint="$2"
    shift # past argument
    shift # past value
    ;;
    -f)
    file="$2"
    shift # past argument
    shift # past value    
    ;; 
    -auth)
    auth="$2"
    shift # past argument
    shift # past value    
    ;;
    *)  # unknown option
    shift # past argument
    ;;
esac
done

# endpoint
if [ -z "$endpoint" ];
then
    endpoint=https://live.european-language-grid.eu/execution
    echo "* endpoint was not provided (-end <url>) using default:"$endpoint
else
        echo "* endpoint":$endpoint
fi

# Determine auth
if [ -z "$auth" ];
then
        echo "* Authentication method was not provided (-auth basic|jwt|no)."
        echo "* For basic authentication put user/pass pair in userpass.txt"
	echo "* For jwt authentication put your token in token.txt"
        echo "* no means that authentication is not required"
        exit 0
else
        echo "* auth:"$auth

        if [ "$auth" == "basic" ]; then
                upVal=`cat userpass.txt`
                echo "* userpass:"$upVal" loaded from userpass.txt"
                cmd="curl -k -u $upVal "
        elif [ "$auth" == "jwt" ]; then
                tokenVal=`cat token.txt`
                #echo "tokenVal:"$tokenVal" loaded from token.txt"
                cmd="curl -k -H 'Authorization: Bearer $tokenVal'" 
        elif [ "$auth" == "no" ]; then
                cmd="curl -k "
        else
                echo "* auth method not valid"
        fi
fi

cmdbase=$cmd

# ltsrv
if [ -z "$ltsrv" ];
then
	echo "* LT Service ID was not provided (-ltsrv <LT Service ID>)"
	#kubectl get pods -n elg-srv-dev
	exit 0
else
	echo "* ltsrv:"$ltsrv;
fi

# oper
if [ -z "$oper" ];
then
	echo "* Operation was not provided (-op processText|processAudio)"
	exit 0
else
	echo "* oper:"$oper
        fileNotProvidedMsg="file with data was not provided ... (use -f to specify a file)"
	
	if [ "$oper" == "processText" ] || [ "$oper" == "processStructured" ]; then
		if [ -z "$file" ]; 
		then
			file='./txtEx/EN_default.txt'
			echo "* "$fileNotProvidedMsg" using default $file"
		fi
    		params=" -H 'Content-Type: text/plain' --data  '@"$file"'" 
	elif [ "$oper" == "processAudio" ]; then
		if [ -z "$file" ]; 
		then
                	echo "* "$fileNotProvidedMsg" using default ./wavEx/sample.wav"
			file="./wavEx/sample.wav"
                fi

		if [[ $file == *wav ]] ;
		then	
    			params=" -H 'Content-Type: audio/x-wav' --data-binary '@"$file"'"	
		else
			params=" -H 'Content-Type: audio/audio/mpeg'--data-binary '@"$file"'"	
		fi
    	fi		
fi


#echo "* params:"$params
cmd=$cmd" $params $endpoint/$oper/$ltsrv""; echo ; echo"

echo ""	
echo $cmd 1> firstCall
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Waiting for response ... "
response=$(eval $cmd)
echo -e "\n\nthe response is ..."
echo $response


if [[ $endpoint == *execution ]] ;
then
      echo "DONE!"
else
      echo -e "\nStart asking rest server"	
      # Extract poll URI
      uri=`echo $response | sed 's/.*uri":"//g' | sed 's/".*//g'`
      #echo $uri	
      secs=120                          # Set interval (duration) in seconds.
      endTime=$(( $(date +%s) + secs )) # Calculate end time.
      receivedResp=0

      # Start polling rest server
      while [ $(date +%s) -lt $endTime ] && [ $receivedResp == 0 ] ; do  # Loop until interval has elapsed.
      	# Wait a little bit and try to get the response.
	sleep 3
	echo -e "response is ... \n" 
	response=$(eval $cmdbase" "$uri)
	# Save call
	echo $cmdbase" "$uri 1> pollCall
	# Check if the results are returned. 
	if [[ $response == {\"response\"* ]] || [[ $response == {\"failure\"* ]];
	then
	  receivedResp=1
	fi
	echo $response
	echo -e "\n\n"
      done
fi
