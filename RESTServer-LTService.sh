#!/bin/bash

function pause(){
   read -p "$*"
}

stopAndRemoveContainer(){
	containerName=$1
	echo "--"$containerName
	docker ps | grep $containerName
	if [ $? -eq 0 ];
	then
		echo 'Stopping['$containerName']'
        	docker stop $containerName
		#echo 'Deleting['$containerName']'
        	#docker rm -f $containerName
	fi
}

usage()  
{  
 echo "-norest (excludes REST Server)"
 echo "-nolt (excludes LT Service)"
 echo "-ltimg |img| (specifies the LT Service image)"
 echo "-nopulllt (disables pulling of the LT Service image)" 
 exit 1  
} 

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

clear
echo "~~~~~~~~~~~~~~~~~~"

rest="yes"
lt="yes"
pulllt="yes"

restCont="restS"
restImage="registry.gitlab.com/european-language-grid/ilsp/elg-lt-service-execution-all:develop-reactive"

ltCont="lt"
ltImage="registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:latest"

# Parse arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -nolt)
    lt="no"
    shift # past argument
    ;;
    -norest)
    rest="no"
    shift # past argument
    ;;
    -nopulllt)
    pulllt="no"
    shift # past argument
    ;;
    -help)
    shift # past argument
    usage	 
    ;;	
    -ltimg)
    ltImage=$2
    shift # past argument
    shift # past value
    ;;
    *)  # unknown option
    shift # past argument
    ;;
esac
done

if [  "$rest" == "yes" ];
then
        # Stopping/Deleting rest
        stopAndRemoveContainer $restCont
        # Start REST Server
        docker pull $restImage
        docker run --rm -d --network=host -v $PWD/application.properties:/elg/dockerCmd/application.properties --name $restCont $restImage
        ./wait.sh localhost:8081 --timeout=60 --strict -- echo " rest is up "
        sleep 5
        docker logs $restCont
fi
  
if [  "$lt" == "yes" ];
then
	# Stopping/Deleting LT-Service lt
	stopAndRemoveContainer $ltCont
	# Start 
	if [  "$pulllt" == "yes" ];
	then
		echo "lt pull"
		docker pull $ltImage
	fi
	docker run --rm -d --network=host --name  $ltCont $ltImage
        ./wait.sh localhost:8080 --timeout=60 --strict -- echo " lt is up "
	sleep 5
	docker logs $ltCont
fi

pause 'Press [Enter] key to stop everything and exit...' 

if [  "$rest" == "yes" ];
then
	stopAndRemoveContainer $restCont
fi

if [  "$lt" == "yes" ];
then
        stopAndRemoveContainer $ltCont
fi

