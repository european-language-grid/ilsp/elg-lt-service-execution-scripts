
## ELG LT Service Execution - Overview

Each "LT Service" in ELG is integrated with the rest of the ELG platform
directly via HTTP protocol. Actually on startup an "LT Service" exposes a REST endpoint which follows the ELG specfications. 
When a message arrives to the LT endpoint (e.g. for text, audio processing), it is read and the actual content
is passed to the LT tool which process it and returns a result. 
In front of the "LT service(s)" there is a common server ("LT Service execution orchestrator")
that offers a set of REST endpoints used as a gateway for invoking any backend integrated "LT Service".

"LT Service execution orchestrator" <-> "LT Service" 

Alternatively, an "LT Service" is integrated to the platform ("LT Service execution orchestrator") via a separate 
(auxiliary) "Service Adapter" application.
In such a scenario the "Service adapter" exposes the ELG-compliant REST endpoint and takes the responsibility
to translate/forward the received messages to the actual "LT Service". The responses from the "LT Service"
are transformed by the "Service Adapter" to the ELG format.

"LT Service execution orchestrator" <-> "Service Adapter" <-> "LT Service"

All the software components/modules of ELG (including "LT Service orchestrator",  "LT Service", "Service adapter")
run as Docker containers in a kubernetes (k8s) cluster.

## Steps for integrating your LT service 

1. Create an application ("LT Service" plus "Service adapter" if required).
Your application should expose a REST endpoint,
process the content (in the case of "Service adapter" it calls the "LT Service") and return 
back an appropriate response. 
The request/response messages that are consumed/generated from the "LT Service" (or "Service Adapter")
should follow the ELG specs. More details can be found in the following links:
        * https://gitlab.com/european-language-grid/platform/elg-apis/
        * https://gitlab.com/european-language-grid/platform/elg-apis/tree/master/doc

2. The applications, "LT Service" and "Service adapter" (if there is one) should be packaged in seperate docker images. 

3. Test your LT Application and image. 
If your image and application work as expected then push the image in
a Docker registry (e.g. GitLab's registry or DockerHub). If there is a "Service Adapter"
you should also push the respective image to a registry.  
In the  next section we provide instructions and code for testing
the "LT Service execution orchestrator" <-> "LT Service" scenario.

4. Then we (ELG admins) need the docker image location for "LT Service" (and "Service adapter"). 
We will create the necessary config files (for k8s) 
 and we will deploy yout LT app in our kubernetes cluster. In some cases 
(e.g. proprietary software) a dedicated k8s namespace with restricted access might have to be created. 

5. The LT service will appear in our catalogue (ELG catalogue) if you create a metadata record. The record can be uploaded to the catalogue 
via a REST API. 


NOTE: The steps that are given above  are for integrating a service in the current ELG deployment.
In the future the procedure might change.

## Testing your LT Application and image (locally without kubernetes).

If you have created the LT Application according to the ELG specs and you have packaged it in a Docker image
you can try if it works in your own machive by starting "LT Service Execution orchestator", "LT Service" containers. 
To do that follow the steps below.

1. Install docker 

2. Clone the required scripts repo.

        git clone https://gitlab.com/european-language-grid/ilsp/elg-lt-service-execution-scripts.git

3. Start everything 

        cd elg-lt-service-execution-scripts 

	Configure to the application.properties file the LT tool REST endpoint. E.g.

        elg.ltservices.staticServices.[/lt]=http://localhost:8080/process/ 

    and

        ./RESTServer-LTService.sh -ltimg <your lt image>

    -ltimg specifies the image that contains your LT app.

    E.g run 

       ./RESTServer-LTService.sh -ltimg registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:latest 


    and wait until you see this message 

       "Press [Enter] key to stop everything and exit..."

    Then run in a separate terminal 

       docker ps 

    You should see 2 containers running with names "lt", "restS".
 
    NOTE 1: If you add "-nopulllt" then you disable lt image pulling. 

       ./RESTServer-LTService.sh -nopulllt -ltimg registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:latest 
       
    This way you can create LT images locally at you machine and test them without uploading them and pulling them back.	 

    NOTE 2: Also you can avoid starting "LT Server" every time. You can start it once without starting "LT Service".


       ./RESTServer-LTService.sh -nolt

     Then run "LT service" separately. 
 
       ./RESTServer-LTService.sh -norest -nopulllt -ltimg registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:latest 
	 	
	
4. Test with a REST client

    In a separate terminal

       cd elg-lt-service-execution-scripts/client
       
    and run

       ./ltClient.sh -auth no -ltsrv lt -end 127.0.0.1:8081 -op <REST Server endpoint>
       
    -end specifies where the "LT Service Exec. orchestator" runs at.
    -ltsrv specifies the LT service ID and -op the REST service type that is called (processText|processAudio). 
    -auth specifies the type of authentication that is used to access the "LT Service execution orchestrator" (no means that no authentication is required).
    With -f you can specify the file that contains the data that will be sent for processing. If not specified default files are 
    used.

   E.g. for testing an "LT Service" that processes text you can run the following and wait for result.
 
       ./ltClient.sh -auth no -ltsrv lt -end 127.0.0.1:8081 -op processText

    In the meantime or at any point in a separate terminal you can check
    the logs of the running containers. E.g. 
     
       docker logs -f lt

    and

       docker logs -f restS

    If you get an appropriate response then your LT Service is considered to be compatible with ELG.

